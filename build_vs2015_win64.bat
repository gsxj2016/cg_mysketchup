@echo off
echo Required software:
echo     64-bit Windows Operating System
echo     Microsoft Visual Studio 2015 / Microsoft Visual C++ Build Tool 2015
echo     CMake 3.7+ (With PATH set)
pause
echo on

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
md build_vs2015
cd build_vs2015
cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Release ..
msbuild /p:Configuration=Release ALL_BUILD.vcxproj
copy ..\third_party\SFML-2.4.2-MSVC64\bin\sfml-graphics-2.dll Release\
copy ..\third_party\SFML-2.4.2-MSVC64\bin\sfml-window-2.dll Release\
copy ..\third_party\SFML-2.4.2-MSVC64\bin\sfml-system-2.dll Release\
copy ..\third_party\DroidSansFallbackFull.ttf Release\
start Release\
pause