cmake_minimum_required(VERSION 3.7)
project(MySketchUp CXX)

set(CMAKE_CXX_STANDARD 14)

# Source file configuration
set(EXEC_NAME MySketchUp)
set(TEST_NAME MySketchUp_Test)
set(SRCS_NAME
        src/defs.hpp
        src/openglinc.hpp
        src/app.cpp src/app.hpp
        src/vec3.cpp src/vec3.hpp
        src/geometry.cpp src/geometry.hpp
        src/pointpicker.cpp src/pointpicker.hpp
        src/model.cpp src/model.hpp
        )

add_executable(${EXEC_NAME} ${SRCS_NAME} src/main.cpp)
add_executable(${TEST_NAME} ${SRCS_NAME} test/catch.hpp test/test.cpp)

# Library configuration
if (MSVC)
    set(SFML_ROOT third_party/SFML-2.4.2-MSVC64)
elseif (MINGW)
    set(SFML_ROOT third_party/SFML-2.4.2-MinGW64)
endif ()

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})
find_package(OpenGL REQUIRED)
find_package(SFML 2 REQUIRED system window graphics)

include_directories(${SFML_INCLUDE_DIR})
target_link_libraries(${EXEC_NAME} ${OPENGL_LIBRARIES} ${SFML_LIBRARIES})
target_link_libraries(${TEST_NAME} ${OPENGL_LIBRARIES} ${SFML_LIBRARIES})

# Compile flag configuration
if (CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wnarrowing -Wconversion")
endif ()

if (MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /utf-8")
endif ()

message(STATUS "CXX FLAGS         :${CMAKE_CXX_FLAGS}")
message(STATUS "CXX FLAGS(DEBUG)  :${CMAKE_CXX_FLAGS_DEBUG}")
message(STATUS "CXX FLAGS(RELEASE):${CMAKE_CXX_FLAGS_RELEASE}")

