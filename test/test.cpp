#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "../src/geometry.hpp"

using CG::Line;
using CG::Vec;
using CG::dcmp;
using CG::Plane;

TEST_CASE("Line intersection") {
    Vec p;
    bool ok;

    Line a({0.f, 1.f, 0.f}, {1.f, 0.f, 0.f});
    Line b({2000.f, 2000.f, 0.f}, {0.f, 0.f, 0.f});
    ok = a.intersection(b, p);
    REQUIRE(ok);
    REQUIRE((p - Vec(0.5f, 0.5f, 0.f)).zero());

    Line c({0.f, 1.f, 0.f}, {1.f, 0.f, 0.f});
    Line d({-10.f, 11.f, 5.f}, {10.f, -9.f, 5.f});
    ok = c.intersection(d, p);
    REQUIRE_FALSE(ok);

    Line e({2.f, 2.f, 2.f}, {0.f, -2.f, 0.f});
    Line f({0.f, 0.f, 2.f}, {2.f, 0.f, 0.f});
    ok = e.intersection(f, p);
    REQUIRE(ok);
    REQUIRE((p - Vec(1.f, 0.f, 1.f)).zero());


    Line g({2.f, 1.f, 2.f}, {0.f, -2.f, 0.f});
    Line h({0.f, 0.f, 2.f}, {2.f, 0.f, 0.f});
    ok = g.intersection(h, p);
    REQUIRE_FALSE(ok);
}

TEST_CASE("Projection") {
    Vec p;

    Plane p1({}, {0.f, 1.f, 0.f}, {1.f, 0.f, 0.f});
    p = p1.pointProjection({22.f, 33.f, 88.f});
    REQUIRE((p - Vec(22.f, 33.f, 0.f)).zero());

    Plane p2({1.f, 0.f, 0.f}, {0.f, 0.f, 1.f}, {0.f, 1.f, 1.f});
    p = p2.pointProjection({4.f, 9.f, 5.f});
    REQUIRE((p - Vec(0.f, 9.f, 1.f)).zero());
}
