@echo off
echo Required software:
echo     64-bit Windows
echo     Microsoft Visual Studio 2017 / Microsoft Visual C++ Build Tool 2017
echo     CMake 3.7+ (With PATH set)
pause
echo on

call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat" amd64
md build_vs2017
cd build_vs2017
cmake -G "Visual Studio 15 2017 Win64" -DCMAKE_BUILD_TYPE=Release ..
msbuild /p:Configuration=Release ALL_BUILD.vcxproj
copy ..\third_party\SFML-2.4.2-MSVC64\bin\sfml-graphics-2.dll Release\
copy ..\third_party\SFML-2.4.2-MSVC64\bin\sfml-window-2.dll Release\
copy ..\third_party\SFML-2.4.2-MSVC64\bin\sfml-system-2.dll Release\
copy ..\third_party\DroidSansFallbackFull.ttf Release\
start Release\
pause