#include <iostream>
#include "app.hpp"

int main(int argc, char **argv) {
    if (argc == 2) {
        std::cout << "Loading file: " << argv[1] << std::endl;
        CG::App::instance().run(argv[1]);
    } else {
        CG::App::instance().run();
    }
    return 0;
}