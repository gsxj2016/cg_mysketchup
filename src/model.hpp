#ifndef MYSKETCHUP_MODEL_HPP
#define MYSKETCHUP_MODEL_HPP

#include "geometry.hpp"
#include "openglinc.hpp"
#include <list>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <string>

namespace CG {

    class LineItem {
    public:
        size_t id1, id2;

        LineItem(size_t id1, size_t id2) : id1(id1), id2(id2) {}

        bool operator==(const LineItem &rhs) const {
            return (id1 == rhs.id1 && id2 == rhs.id2) || (id1 == rhs.id2 && id2 == rhs.id1);
        }
    };

    struct PairHash {
        std::hash<size_t> h;

        size_t operator()(const LineItem &x) const {
            return h(x.id1) ^ h(x.id2);
        }
    };

    class PointItem {
    private:
        static size_t getNewId();

    public:
        const size_t id;
        GLuint tempId;
        Vec pt;

        explicit PointItem(const Vec &_pt);

    };

    const int HL_ROLE_PL0 = 0;
    const int HL_ROLE_PL1 = 1;
    const int HL_ROLE_PL2 = 2;
    const int HL_ROLE_SLA = 3;
    const int HL_ROLE_SLB = 4;
    const int HL_ROLE_SEL = 5;
    const int HL_ROLE_HIT = 6;
    const int HL_ROLESALL = 7;
    const GLfloat HL_COL_R[HL_ROLESALL] = {0.7f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f};
    const GLfloat HL_COL_G[HL_ROLESALL] = {0.7f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.5f};
    const GLfloat HL_COL_B[HL_ROLESALL] = {1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f};

    class DataModel {
    public:
        std::unordered_map<size_t, PointItem> points;
        std::unordered_set<LineItem, PairHash> lines;

        DataModel();

        DataModel(const DataModel &) = delete;

        DataModel &operator=(const DataModel &)= delete;

        /**
         * 添加点（API）
         * @param pt 点坐标
         * @return 新增的点的ID
         */
        size_t newPoint(const Vec &pt);

        /**
         * 删除点（如果存在）（API）
         * @param id 要删除的点的ID
         */
        void erasePoint(size_t id);

        /**
         * 添加线段（API）
         * @param a 第一个端点ID
         * @param b 第二个端点ID
         */
        void newLine(size_t a, size_t b);

        /**
         * 删除线段（如果存在）（API）
         * @param a 第一个端点ID
         * @param b 第二个端点ID
         */
        void eraseLine(size_t a, size_t b);

        /**
         * 等分线段（如果对应线段存在）（API）
         * @param a 线段第一个端点ID
         * @param b 线段第二个端点ID
         * @param cnt 等分段数
         */
        void divideLine(size_t a, size_t b, int cnt);

        /**
         * 延ab方向延长线段（如果对应线段存在）（API）
         * @param a 线段第一个端点ID
         * @param b 线段第二个端点ID
         * @param cnt 延长为原来的几倍
         */
        void extendLine(size_t a, size_t b, int cnt);

        /**
         * 拾取已有点（API）
         * @param pick 鼠标坐标对应的射线
         * @return 取到的点的ID，失败则返回0
         */
        size_t pickNearPoint(const Line &pick);

        /**
         * 调用OpenGL绘图（API）
         * @param all 是否绘制所有点线（否则只绘制线框）
         */
        void draw(bool all);

        /**
         * 更新点高亮状态（API）
         * @param role 高亮点类型（平面参考点x3、线段求交时第一条线段的两个端点、当前点、指向点）
         * @param id 高亮点ID（0表示取消某个高亮）
         */
        void updateHighlight(int role, size_t id);

        /**
         * 线段求交并从交点处（若存在）分割线段（API）
         * @param a 第一条线段第一个端点ID
         * @param b 第一条线段第二个端点ID
         * @param c 第二条线段第一个端点ID
         * @param d 第二条线段第二个端点ID
         */
        void findIntersection(size_t a, size_t b, size_t c, size_t d);

        /**
         * 寻找处于某个位置的点（API）
         * @param pt 坐标
         * @return 点ID（0表示不存在）
         */
        size_t findPointByPos(const Vec &pt);

        /**
         * 保存模型（API）
         * @param fileName 文件路径和文件名
         */
        void save(const char *fileName);

        /**
         * 读取模型（API）
         * @param fileName 文件路径和文件名
         */
        void load(const char *fileName);

    private:
        std::vector<GLfloat> vertexArray;
        std::vector<GLfloat> vertexColor;
        std::vector<GLuint> indicesLines;
        size_t hlList[HL_ROLESALL];

        /**
         * 清除无效点（内部函数）
         */
        void arrayCleanUp();

        /**
         * 更新顶点数组（供OpenGL使用）（内部函数）
         */
        void updateVertexArray();

        /**
         * 更新线段端点编号数组（供OpenGL使用，需先调用arrayCleanUp）（内部函数）
         */
        void updateLineIndices();

        /**
         * 数据结构维护（调用任何会改变点线的API后会自动调用）（内部函数）
         */
        void updateGLArrays();

        /**
         * 设置高亮颜色（内部函数）
         */
        void setColor(size_t id, GLfloat r, GLfloat g, GLfloat b);

        /**
         * 依次设置所有高亮颜色（内部函数）
         */
        void markAllHighlight();

        /**
         * 从交点处切分线段（内部函数）
         */
        void applySplit(std::unordered_set<LineItem, PairHash>::iterator it,
                        std::unordered_map<size_t, PointItem>::iterator ita,
                        std::unordered_map<size_t, PointItem>::iterator itb,
                        std::unordered_map<size_t, PointItem>::iterator itc);

        /**
         *  判断点是否在线段端点上（内部函数）
         */
        size_t onTerminal(std::unordered_map<size_t, PointItem>::iterator ita,
                          std::unordered_map<size_t, PointItem>::iterator itb,
                          const Vec &p);

    };

}

#endif //MYSKETCHUP_MODEL_HPP
