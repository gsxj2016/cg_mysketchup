#include "app.hpp"
#include <iostream>
#include <cassert>
#include <sstream>
#include <cstring>

namespace CG {

    App &App::instance() {
        static App app;
        return app;
    }

    App::App() : currPoint(0),
                 hitPoint(0),
                 planeCnt(0),
                 mainMode(MODE_DRAW_POINT),
                 pickPlane(PICK_PLANE_XY),
                 okLine(false),
                 pickLinePoint(false),
                 noPickFree(false),
                 orthographic(false),
                 renderLinesOnly(false),
                 mouseX(0),
                 mouseY(0) {
        static bool initialized(false);
        if (initialized)std::abort();
        initialized = true;

        GLfloat iden[16] = {
                1.f, 0.f, 0.f, 0.f,
                0.f, 1.f, 0.f, 0.f,
                0.f, 0.f, 1.f, 0.f,
                0.f, 0.f, 0.f, 1.f
        };
        memcpy(mRot, iden, sizeof(iden));
        pTra[0] = pTra[1] = pTra[2] = 0.f;
        idpp[0] = idpp[1] = idpp[2] = 0;
        sla = slb = 0;
        orthoSize = 150.f;
    }

    void App::run(const char *openFileName) {
        sf::ContextSettings settings;
        settings.depthBits = 24;
        settings.antialiasingLevel = 4;
        settings.majorVersion = 1;
        settings.minorVersion = 1;
        sf::RenderWindow window(sf::VideoMode(WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT), "MySketchUp",
                                sf::Style::Default, settings);
        window.setVerticalSyncEnabled(true);
        window.setActive(true);

        initGLState();
        initModelViewMatrix();
        updateProjectionMatrix(WINDOW_INITIAL_WIDTH, WINDOW_INITIAL_HEIGHT);

        sf::Font font;
        if (!font.loadFromFile("DroidSansFallbackFull.ttf")) {
            std::cerr << "Failed to load font DroidSansFallbackFull.ttf" << std::endl;
            assert(false);
        }
        sf::Text text("", font, 16);
        text.setFillColor(sf::Color::Black);
        updateInfoText();

        if (openFileName) {
            fileName = std::string(openFileName);
            data.load(openFileName);
        } else {
            fileName = std::string("model.txt");
        }

        sf::Clock clock;
        bool winSizeChanged = false;
        float newWinX = WINDOW_INITIAL_WIDTH;
        float newWinY = WINDOW_INITIAL_HEIGHT;

        while (window.isOpen()) {
            sf::Event event;
            while (window.pollEvent(event)) {
                switch (event.type) {
                    case sf::Event::Closed: {
                        window.close();
                        break;
                    }
                    case sf::Event::Resized: {
                        auto _ws = window.getSize();
                        winSizeChanged = true;
                        newWinX = (float) _ws.x;
                        newWinY = (float) _ws.y;
                        updateProjectionMatrix(_ws.x, _ws.y);
                        break;
                    }
                    case sf::Event::MouseButtonReleased: {
                        if (event.mouseButton.button == sf::Mouse::Left) {
                            processMouseLeft(event.mouseButton.x, event.mouseButton.y);
                        } else if (event.mouseButton.button == sf::Mouse::Right) {
                            processMouseRight(event.mouseButton.x, event.mouseButton.y);
                        }
                        break;
                    }
                    case sf::Event::MouseMoved: {
                        processMouseMove(event.mouseMove.x, event.mouseMove.y);
                        break;
                    }
                    case sf::Event::KeyReleased: {

                        switch (event.key.code) {
                            case sf::Keyboard::Key::F:
                                switchAxisPlane();
                                break;
                            case sf::Keyboard::Key::C:
                                setCurrentPoint(0);
                                break;
                            case sf::Keyboard::Key::R:
                                resetView();
                                break;
                            case sf::Keyboard::Key::Num1:
                                mainMode = MODE_DRAW_POINT;
                                processModeSwitch();
                                break;
                            case sf::Keyboard::Key::Num2:
                                mainMode = MODE_DRAW_LINE;
                                processModeSwitch();
                                break;
                            case sf::Keyboard::Key::Num3:
                                mainMode = MODE_ERASE_POINT;
                                processModeSwitch();
                                break;
                            case sf::Keyboard::Key::Num4:
                                mainMode = MODE_ERASE_LINE;
                                processModeSwitch();
                                break;
                            case sf::Keyboard::Key::Num5:
                                mainMode = MODE_DIVIDE;
                                processModeSwitch();
                                break;
                            case sf::Keyboard::Key::Num6:
                                mainMode = MODE_EXTEND;
                                processModeSwitch();
                                break;
                            case sf::Keyboard::Key::Num7:
                                mainMode = MODE_INTERSECTION;
                                processModeSwitch();
                                break;
                            case sf::Keyboard::Key::LBracket:
                                if (divideCnt > 2)--divideCnt;
                                updateInfoText();
                                break;
                            case sf::Keyboard::Key::RBracket:
                                ++divideCnt;
                                updateInfoText();
                                break;
                            case sf::Keyboard::Key::Slash:
                                orthographic = !orthographic;
                                updateProjectionMatrix(window.getSize().x, window.getSize().y);
                                break;
                            case sf::Keyboard::Key::O:
                                data.save(fileName.data());
                                break;
                            case sf::Keyboard::Key::V:
                                markPointOnPlane();
                                break;
                            default:
                                break;
                        }

                        break;
                    }
                    default:
                        break;
                }
            }

            float et = clock.restart().asSeconds();
            movingControl(et);
            rotationControl(et);
            if (orthographic)orthographicViewControl(et, window.getSize());
            checkPickingMode();

            drawGLContent();

            //======================================
            window.pushGLStates();
            if (infoUpdated) {
                infoUpdated = false;
                text.setString(infoText);
            }
            if (winSizeChanged) {
                winSizeChanged = false;
                sf::Vector2f _siz(newWinX, newWinY);
                sf::View view(_siz * 0.5f, _siz);
                window.setView(view);
            }
            window.draw(text);
            window.popGLStates();
            //======================================

            window.display();
        }

    }

    void App::initGLState() {
        glEnable(GL_DEPTH_TEST);
        glClearColor(1.f, 1.f, 1.f, 1.f);
    }

    void App::drawGLContent() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if (renderLinesOnly) {
            data.draw(false);
        } else {
            drawAxis();
            data.draw(true);
            drawIndicator();
        }
    }

    void App::updateProjectionMatrix(GLsizei width, GLsizei height) {
        glViewport(0, 0, width, height);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        GLdouble aspect = height ? GLdouble(width) / height : 1.;
        if (orthographic) {
            glOrtho(-orthoSize * aspect, orthoSize * aspect,
                    -orthoSize, orthoSize,
                    1., 10000.);
        } else {
            gluPerspective(45., aspect, 1., 10000.);
        }
    }

    void App::initModelViewMatrix() {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(200., -200., 200., 0., 0., 0., 0., 0., 300.);
        glPushMatrix();
        glPushMatrix();
    }

    void App::drawAxis() {
        glLineWidth(1.5f);
        glPointSize(5.f);
        static GLfloat axisVertices[] = {
                -COORD_MAX, 0.f, 0.f,
                0.f, 0.f, 0.f,
                COORD_MAX, 0.f, 0.f,
                0.f, 0.f, 0.f,
                0.f, -COORD_MAX, 0.f,
                0.f, 0.f, 0.f,
                0.f, COORD_MAX, 0.f,
                0.f, 0.f, 0.f,
                0.f, 0.f, -COORD_MAX,
                0.f, 0.f, 0.f,
                0.f, 0.f, COORD_MAX,
                0.f, 0.f, 0.f,
                0.f, 0.f, 0.f
        };
        static GLfloat axisColors[] = {
                .5f, 0.f, 0.f,
                .5f, 0.f, 0.f,
                1.f, 0.f, 0.f,
                1.f, 0.f, 0.f,
                0.f, 0.f, .5f,
                0.f, 0.f, .5f,
                0.f, 0.f, 1.f,
                0.f, 0.f, 1.f,
                0.f, .5f, 0.f,
                0.f, .5f, 0.f,
                0.f, 1.f, 0.f,
                0.f, 1.f, 0.f,
                0.f, 0.f, 0.f
        };
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, axisVertices);
        glColorPointer(3, GL_FLOAT, 0, axisColors);
        glDrawArrays(GL_LINES, 0, 12);
        glDrawArrays(GL_POINTS, 12, 1);
        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
    }

    void App::drawIndicator() {
        static GLfloat defaultPlaneVertex[] = {
                0.f, 0.f, 0.f,
                9.f, 0.f, 0.f,
                9.f, 9.f, 0.f,
                0.f, 9.f, 0.f,
                0.f, 0.f, 0.f,
                0.f, 9.f, 0.f,
                0.f, 9.f, 9.f,
                0.f, 0.f, 9.f,
                0.f, 0.f, 0.f,
                0.f, 0.f, 9.f,
                9.f, 0.f, 9.f,
                9.f, 0.f, 0.f
        };
        glLineWidth(1.f);
        glColor3f(1.f, 0.f, 1.f);
        if (pickPlane == PICK_PLANE_CUSTOM) {
            if (!nowPlane.vaild())return;
            glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(3, GL_FLOAT, 0, planeVertex);
            glDrawArrays(GL_LINE_LOOP, 0, 3);
            glDisableClientState(GL_VERTEX_ARRAY);
        } else {
            glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(3, GL_FLOAT, 0, defaultPlaneVertex);
            glDrawArrays(GL_LINE_LOOP, 4 * pickPlane, 4);
            glDisableClientState(GL_VERTEX_ARRAY);
        }

        if (pickLinePoint && okLine) {
            glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(3, GL_FLOAT, 0, planeVertex);
            glDrawArrays(GL_LINES, 3, 2);
            glDisableClientState(GL_VERTEX_ARRAY);
        }
    }

    void App::rotateView(GLfloat deg, GLfloat x, GLfloat y, GLfloat z) {
        //switch matrix
        glMatrixMode(GL_MODELVIEW);
        //update rotate matrix
        glLoadMatrixf(mRot);
        glRotatef(deg, x, y, z);
        glGetFloatv(GL_MODELVIEW_MATRIX, mRot);
        //apply modelview (stack: camera-translate-rotate)
        glPopMatrix();
        glPushMatrix(), glMultMatrixf(mRot);
    }

    void App::moveView(GLfloat x, GLfloat y, GLfloat z) {
        //update position
        pTra[0] += x, pTra[1] += y, pTra[2] += z;
        //switch matrix
        glMatrixMode(GL_MODELVIEW);
        //apply modelview (stack: camera-translate-rotate)
        glPopMatrix();
        glPopMatrix();
        glPushMatrix(), glTranslatef(pTra[0], pTra[1], pTra[2]);
        glPushMatrix(), glMultMatrixf(mRot);
    }

    void App::resetView() {
        //switch matrix
        glMatrixMode(GL_MODELVIEW);
        //reset rotation
        glLoadIdentity();
        glGetFloatv(GL_MODELVIEW_MATRIX, mRot);
        //reset position
        pTra[0] = pTra[1] = pTra[2] = 0.f;
        //apply modelview (stack: camera-translate-rotate)
        glPopMatrix(), glPopMatrix();
        glPushMatrix(), glPushMatrix();
    }

    void App::movingControl(float et) {
        int x = 0, y = 0, z = 0;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q)) --x;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D)) ++x;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A)) --y;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::E)) ++y;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S)) --z;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W)) ++z;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::SemiColon)) --x, ++y, --z;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Quote)) ++x, --y, ++z;
        if (x || y || z) {
            moveView(et * MOVE_PER_SEC * GLfloat(x), et * MOVE_PER_SEC * GLfloat(y), et * MOVE_PER_SEC * GLfloat(z));
        }
    }

    void App::rotationControl(float et) {
        int z = 0, x = 0, y = 0;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Z)) --z;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::X)) ++z;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Left)) --x;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Right)) ++x;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Down)) --y;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Up)) ++y;
        if (z) rotateView(et * ROT_PER_SEC * GLfloat(z), 0.f, 0.f, 1.f);
        if (x) rotateView(et * ROT_PER_SEC * GLfloat(x), 1.f, 0.f, 0.f);
        if (y) rotateView(et * ROT_PER_SEC * GLfloat(y), 0.f, 1.f, 0.f);
    }

    void App::orthographicViewControl(float et, sf::Vector2u ws) {
        int d = 0;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Comma)) --d;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Period)) ++d;
        if (d) {
            orthoSize -= et * ORTHO_MOVE_PER_SEC * GLfloat(d);
            if (orthoSize <= 10.f)orthoSize = 10.f;
            updateProjectionMatrix(ws.x, ws.y);
        }
    }

    void App::processMouseLeft(int x, int y) {
        switch (mainMode) {
            case MODE_DRAW_POINT: {
                if (hitPoint) setCurrentPoint(hitPoint);
                else {
                    Vec p;
                    bool ok = pickFreePoint(x, y, p);
                    if (ok) setCurrentPoint(data.newPoint(p));
                }
                break;
            }
            case MODE_DRAW_LINE: {
                if (hitPoint) {
                    if (currPoint)data.newLine(currPoint, hitPoint);
                    setCurrentPoint(hitPoint);
                } else {
                    Vec p;
                    bool ok = pickFreePoint(x, y, p);
                    if (ok) {
                        size_t np = data.newPoint(p);
                        if (currPoint)data.newLine(currPoint, np);
                        setCurrentPoint(np);
                    }
                }
                break;
            }
            case MODE_ERASE_POINT: {
                if (hitPoint) {
                    data.erasePoint(hitPoint);
                }
                setCurrentPoint(0);
                break;
            }
            case MODE_ERASE_LINE: {
                if (hitPoint) {
                    if (currPoint) {
                        data.eraseLine(hitPoint, currPoint);
                        setCurrentPoint(0);
                    } else {
                        setCurrentPoint(hitPoint);
                    }
                } else {
                    setCurrentPoint(0);
                }
                break;
            }
            case MODE_DIVIDE: {
                if (currPoint) {
                    if (hitPoint) {
                        data.divideLine(currPoint, hitPoint, divideCnt);
                    }
                    setCurrentPoint(0);
                } else {
                    setCurrentPoint(hitPoint);
                }
                break;
            }
            case MODE_EXTEND: {
                if (currPoint) {
                    if (hitPoint) {
                        data.extendLine(currPoint, hitPoint, divideCnt);
                    }
                    setCurrentPoint(0);
                } else {
                    setCurrentPoint(hitPoint);
                }
                break;
            }
            case MODE_INTERSECTION: {
                if (hitPoint) {
                    if (currPoint) {
                        if (sla) {
                            data.findIntersection(sla, slb, hitPoint, currPoint);
                            updateFirstLineSelection(0, 0);
                        } else {
                            updateFirstLineSelection(hitPoint, currPoint);
                        }
                        setCurrentPoint(0);
                    } else {
                        setCurrentPoint(hitPoint);
                    }
                } else {
                    setCurrentPoint(0);
                }
                break;
            }
            default:
                assert(false);
                std::abort();
        }
    }

    void App::processMouseRight(int, int) {
        if (hitPoint) addPlaneReferencePoint(hitPoint);
    }

    void App::processMouseMove(int x, int y) {
        mouseX = x;
        mouseY = y;
        size_t id = data.pickNearPoint(pointPicker.getPickLine((GLfloat) x, (GLfloat) y));
        data.updateHighlight(HL_ROLE_HIT, hitPoint = id);
    }

    void App::switchAxisPlane() {
        pickPlane = (pickPlane + 1) % WRAP_PICK_PLANE;
        data.updateHighlight(HL_ROLE_PL0, 0);
        data.updateHighlight(HL_ROLE_PL1, 0);
        data.updateHighlight(HL_ROLE_PL2, 0);
        updateTargetLine();
    }

    Plane App::getActivePlane() {
        switch (pickPlane) {
            case PICK_PLANE_XY:
                return Plane({}, {1.f, 0.f, 0.f}, {0.f, 1.f, 0.f});
            case PICK_PLANE_YZ:
                return Plane({}, {0.f, 1.f, 0.f}, {0.f, 0.f, 1.f});
            case PICK_PLANE_ZX:
                return Plane({}, {0.f, 0.f, 1.f}, {1.f, 0.f, 0.f});
            case PICK_PLANE_CUSTOM:
                return nowPlane;
            default:
                assert(false);
                std::abort();
        }
    }

    void App::setCurrentPoint(size_t id) {
        data.updateHighlight(HL_ROLE_SEL, currPoint = id);
        processMouseMove(mouseX, mouseY);
        updateTargetLine();
        updateInfoText();
    }

    void App::addPlaneReferencePoint(size_t id) {
        auto it = data.points.find(id);
        if (it == data.points.end())return;
        idpp[0] = idpp[1], idpp[1] = idpp[2], idpp[2] = id;
        ptpp[0] = ptpp[1], ptpp[1] = ptpp[2], ptpp[2] = it->second.pt;
        if (planeCnt < 3)++planeCnt;
        if (planeCnt == 3) {
            data.updateHighlight(HL_ROLE_PL0, idpp[0]);
            data.updateHighlight(HL_ROLE_PL1, idpp[1]);
            data.updateHighlight(HL_ROLE_PL2, idpp[2]);
            nowPlane = Plane(ptpp[0], ptpp[1], ptpp[2]);
            pickPlane = PICK_PLANE_CUSTOM;
            planeVertex[0] = ptpp[0].x, planeVertex[1] = ptpp[0].y, planeVertex[2] = ptpp[0].z;
            planeVertex[3] = ptpp[1].x, planeVertex[4] = ptpp[1].y, planeVertex[5] = ptpp[1].z;
            planeVertex[6] = ptpp[2].x, planeVertex[7] = ptpp[2].y, planeVertex[8] = ptpp[2].z;
            updateTargetLine();
        }
    }

    bool App::pickFreePoint(int x, int y, Vec &p) {
        if (noPickFree)return false;
        if (pickLinePoint) {
            return okLine && pointPicker.pickPoint(nowLine, x, y, p);
        } else {
            return pointPicker.pickPoint(getActivePlane(), x, y, p);
        }
    }

    void App::updateTargetLine() {
        auto it = data.points.find(currPoint);
        if (it == data.points.end()) {
            okLine = false;
            return;
        }
        Vec point = it->second.pt;

        Plane plane = getActivePlane();
        okLine = pointPicker.getTargetLine(plane, point, nowLine);
        if (okLine) {
            Vec nd = nowLine.dir().normalize();
            Vec a = nowLine.a - nd * 5000.f;
            Vec b = nowLine.b + nd * 5000.f;
            planeVertex[9] = a.x;
            planeVertex[10] = a.y;
            planeVertex[11] = a.z;
            planeVertex[12] = b.x;
            planeVertex[13] = b.y;
            planeVertex[14] = b.z;
        }
    }

    void App::checkPickingMode() {
        pickLinePoint = sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift);
        noPickFree = sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LControl);
        renderLinesOnly = sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Tab);
    }

    void App::processModeSwitch() {
        if (mainMode >= MODE_ERASE_LINE)setCurrentPoint(0);
        updateFirstLineSelection(0, 0);
        divideCnt = 2;
        updateInfoText();
    }

    void App::updateFirstLineSelection(size_t a, size_t b) {
        if (data.lines.count(LineItem(a, b))) {
            data.updateHighlight(HL_ROLE_SLA, sla = a);
            data.updateHighlight(HL_ROLE_SLB, slb = b);
        } else {
            sla = slb = 0;
            data.updateHighlight(HL_ROLE_SLA, 0);
            data.updateHighlight(HL_ROLE_SLB, 0);
        }
    }

    void App::updateInfoText() {
        std::ostringstream ss;
        switch (mainMode) {
            case MODE_DRAW_POINT:
                ss << "Mode: Draw Point" << std::endl;
                break;
            case MODE_DRAW_LINE:
                ss << "Mode: Draw Line" << std::endl;
                break;
            case MODE_ERASE_POINT:
                ss << "Mode: Erase Point" << std::endl;
                break;
            case MODE_ERASE_LINE:
                ss << "Mode: Erase Line" << std::endl;
                break;
            case MODE_DIVIDE:
                ss << "Mode: Divide Line [" << divideCnt << "]" << std::endl;
                break;
            case MODE_EXTEND:
                ss << "Mode: Extend Line [" << divideCnt << "]" << std::endl;
                break;
            case MODE_INTERSECTION:
                ss << "Mode: Find Intersection" << std::endl;
                break;
            default:
                assert(false);
                abort();
        }
        if (currPoint) {
            auto it = data.points.find(currPoint);
            if (it != data.points.end()) {
                const Vec &p = it->second.pt;
                ss.setf(std::ios_base::fixed, std::ios_base::floatfield);
                ss.precision(3);
                ss << "Selected :( " << p.x << " , " << p.y << " , " << p.z << " )" << std::endl;
            }
        }
        infoText = ss.str();
        infoUpdated = true;
    }

    void App::markPointOnPlane() {
        if (pickLinePoint && okLine) {
            Vec pt;
            bool ok = getActivePlane().intersection(nowLine, pt);
            if (ok) {
                size_t id = data.findPointByPos(pt);
                if (!id) id = data.newPoint(pt);
                setCurrentPoint(id);
            }
        }
        if (!pickLinePoint && sla) {
            Vec pt;
            auto ita = data.points.find(sla);
            auto itb = data.points.find(slb);
            if (ita != data.points.end() && itb != data.points.end()) {
                Line line(ita->second.pt, itb->second.pt);
                bool ok = getActivePlane().intersection(line, pt);
                if (ok) {
                    size_t id = data.findPointByPos(pt);
                    if (!id) id = data.newPoint(pt);
                    setCurrentPoint(id);
                }
            }
            updateFirstLineSelection(0, 0);
        }
    }

}