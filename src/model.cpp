#include <cassert>
#include <vector>
#include <algorithm>
#include <fstream>
#include <iostream>
#include "model.hpp"

namespace CG {

    size_t PointItem::getNewId() {
        static size_t freeId(0);
        return ++freeId;
    }

    PointItem::PointItem(const Vec &_pt) : id(getNewId()), pt(_pt) {}

    size_t DataModel::pickNearPoint(const Line &pick) {
        std::vector<std::pair<Vec, size_t> > canPick;
        for (auto &item : points) {
            const Vec &p = item.second.pt;
            if (pick.distanceToLine(p) < PICK_DIST
                && dcmp((p - pick.a) * pick.dir()) >= 0)
                canPick.emplace_back(p, item.first);
        }
        if (canPick.empty())return 0;
        for (auto &item:canPick) {
            item.first.x = (pick.a - item.first).len();
        }
        return std::min_element(
                canPick.begin(), canPick.end(),
                [](const std::pair<Vec, size_t> &lhs, const std::pair<Vec, size_t> &rhs) {
                    return lhs.first.x < rhs.first.x;
                })->second;
    }

    void DataModel::arrayCleanUp() {
        for (auto it = lines.begin(); it != lines.end(); /*no incr*/) {
            if (!points.count(it->id1) || !points.count(it->id2))
                it = lines.erase(it);
            else
                ++it;
        }
    }

    void DataModel::updateVertexArray() {
        vertexArray.resize(points.size() * 3);
        vertexColor.resize(points.size() * 3);
        GLfloat *ptr = vertexArray.data();
        GLfloat *col = vertexColor.data();
        GLuint id = 0;
        for (auto &item: points) {
            *(ptr++) = item.second.pt.x;
            *(ptr++) = item.second.pt.y;
            *(ptr++) = item.second.pt.z;
            *(col++) = 0.0f;
            *(col++) = 0.0f;
            *(col++) = 1.0f;
            item.second.tempId = id++;
        }
    }

    void DataModel::updateLineIndices() {
        indicesLines.resize(lines.size() * 2);
        GLuint *ptr = indicesLines.data();
        for (auto &item:lines) {
            //assume the existence
            //call arrayCleanUp() first!!
            *(ptr++) = points.find(item.id1)->second.tempId;
            *(ptr++) = points.find(item.id2)->second.tempId;
        }
    }

    void DataModel::draw(bool all) {
        glPointSize(POINT_DRAW_SIZE);
        glLineWidth(2.f);
        glColor3f(0.f, 0.f, 0.f);

        glEnableClientState(GL_VERTEX_ARRAY);

        //prepare
        glVertexPointer(3, GL_FLOAT, 0, vertexArray.data());
        glColorPointer(3, GL_FLOAT, 0, vertexColor.data());

        //draw lines
        glDrawElements(GL_LINES, (GLuint) indicesLines.size(), GL_UNSIGNED_INT, indicesLines.data());

        //draw points
        if (all) {
            glEnableClientState(GL_COLOR_ARRAY);
            glDrawArrays(GL_POINTS, 0, (GLuint) points.size());
            glDisableClientState(GL_COLOR_ARRAY);
        }

        glDisableClientState(GL_VERTEX_ARRAY);
    }

    void DataModel::updateGLArrays() {
        arrayCleanUp();
        updateVertexArray();
        updateLineIndices();
        markAllHighlight();
    }

    void DataModel::updateHighlight(int role, size_t id) {
        if (hlList[role] == id)return;
        setColor(hlList[role], 0.f, 0.f, 1.f);
        hlList[role] = id;
        markAllHighlight();
    }

    DataModel::DataModel() {
        std::fill(hlList, hlList + HL_ROLESALL, 0);
    }

    void DataModel::setColor(size_t id, GLfloat r, GLfloat g, GLfloat b) {
        auto it = points.find(id);
        if (it == points.end())return;
        GLfloat *ptr = vertexColor.data() + 3 * it->second.tempId;
        *(ptr++) = r, *(ptr++) = g, *ptr = b;
    }

    void DataModel::markAllHighlight() {
        for (int i = 0; i < HL_ROLESALL; ++i)
            setColor(hlList[i], HL_COL_R[i], HL_COL_G[i], HL_COL_B[i]);
    }

    size_t DataModel::newPoint(const Vec &pt) {
        if (pt.isInvalid()) return 0;
        PointItem p(pt);
        auto result = points.insert(std::make_pair(p.id, p));
        assert(result.second);
        assert(p.id);
        updateGLArrays();
        return p.id;
    }

    void DataModel::newLine(size_t a, size_t b) {
        if (a == b)return;
        lines.insert(LineItem(a, b));
        updateGLArrays();
    }

    void DataModel::erasePoint(size_t id) {
        points.erase(id);
        updateGLArrays();
    }

    void DataModel::eraseLine(size_t a, size_t b) {
        auto it = lines.find(LineItem(a, b));
        if (it == lines.end())return;
        lines.erase(it);
        updateGLArrays();
    }

    void DataModel::divideLine(size_t a, size_t b, int cnt) {
        if (cnt < 2) return;
        auto it = lines.find(LineItem(a, b));
        if (it == lines.end())return;
        lines.erase(it);

        Vec p1 = points.find(a)->second.pt;
        Vec p2 = points.find(b)->second.pt;
        Vec d = (p2 - p1) / float(cnt);
        Vec last = p1;
        size_t lid = a;
        for (int i = 1; i < cnt; ++i) {
            Vec now = last + d;
            assert(!now.isInvalid());
            PointItem item(now);
            size_t nid = item.id;
            assert(item.id);
            auto insRes = points.insert(std::make_pair(item.id, item));
            assert(insRes.second);

            lines.insert(LineItem(lid, nid));
            last = now;
            lid = nid;
        }
        lines.insert(LineItem(lid, b));
        updateGLArrays();
    }

    void DataModel::extendLine(size_t a, size_t b, int cnt) {
        if (cnt < 2) return;
        auto it = lines.find(LineItem(a, b));
        if (it == lines.end())return;

        Vec p1 = points.find(a)->second.pt;
        Vec p2 = points.find(b)->second.pt;
        Vec d = p2 - p1;
        Vec last = p2;
        size_t lid = b;
        for (int i = 2; i <= cnt; ++i) {
            Vec now = last + d;
            assert(!now.isInvalid());
            PointItem item(now);
            size_t nid = item.id;
            assert(item.id);
            auto insRes = points.insert(std::make_pair(item.id, item));
            assert(insRes.second);

            lines.insert(LineItem(lid, nid));
            last = now;
            lid = nid;
        }
        updateGLArrays();
    }

    void DataModel::findIntersection(size_t a, size_t b, size_t c, size_t d) {
        auto itl1 = lines.find(LineItem(a, b));
        if (itl1 == lines.end())return;
        auto itl2 = lines.find(LineItem(c, d));
        if (itl2 == lines.end())return;

        auto ita = points.find(a);
        auto itb = points.find(b);
        auto itc = points.find(c);
        auto itd = points.find(d);
        Vec is;
        bool ok = Line(ita->second.pt, itb->second.pt)
                .intersection(Line(itc->second.pt, itd->second.pt), is);
        if (!ok)return;
        size_t pab = onTerminal(ita, itb, is);
        size_t pcd = onTerminal(itc, itd, is);
        if (pab && pcd)return;
        if (pab) {
            auto itp = (pab == ita->first) ? ita : itb;
            applySplit(itl2, itc, itd, itp);
        } else if (pcd) {
            auto itp = (pcd == itc->first) ? itc : itd;
            applySplit(itl1, ita, itb, itp);
        } else {
            PointItem pis(is);
            auto insRes = points.insert(std::make_pair(pis.id, pis));
            assert(insRes.second);
            applySplit(itl1, ita, itb, insRes.first);
            applySplit(itl2, itc, itd, insRes.first);
        }
        updateGLArrays();
    }

    size_t DataModel::onTerminal(std::unordered_map<size_t, PointItem>::iterator ita,
                                 std::unordered_map<size_t, PointItem>::iterator itb, const Vec &p) {
        if ((p - ita->second.pt).zero())return ita->first;
        if ((p - itb->second.pt).zero())return itb->first;
        return 0;
    }

    void DataModel::applySplit(std::unordered_set<LineItem, PairHash>::iterator it,
                               std::unordered_map<size_t, PointItem>::iterator ita,
                               std::unordered_map<size_t, PointItem>::iterator itb,
                               std::unordered_map<size_t, PointItem>::iterator itc) {
        Vec a = ita->second.pt;
        Vec b = itb->second.pt;
        Vec c = itc->second.pt;
        if (dcmp((b - a) * (c - a)) > 0 && dcmp((a - b) * (c - b)) > 0) {
            lines.erase(it);
            lines.insert(LineItem(ita->first, itc->first));
            lines.insert(LineItem(itb->first, itc->first));
        }
    }

    void DataModel::save(const char *file) {
        std::ofstream fos;
        fos.open(file);
        if (fos) {
            for (auto &item : points) {
                const Vec &p = item.second.pt;
                fos << "P " << item.first << ' ' << p.x << ' ' << p.y << ' ' << p.z << std::endl;
            }
            for (auto &item : lines) {
                fos << "L " << item.id1 << ' ' << item.id2 << std::endl;
            }
            std::cout << "File saved: " << file << std::endl;
        } else {
            std::cout << "Save failed." << std::endl;
        }
    }

    void DataModel::load(const char *fileName) {
        std::unordered_map<size_t, size_t> keyMap;
        std::ifstream fis;
        fis.open(fileName);
        if (fis) {
            points.clear();
            lines.clear();
            std::string cmd;
            while (fis >> cmd) {
                if (cmd == std::string("P")) {
                    size_t oldId;
                    float x, y, z;
                    if (fis >> oldId >> x >> y >> z) {
                        PointItem item(Vec(x, y, z));
                        keyMap.insert(std::make_pair(oldId, item.id));
                        points.insert(std::make_pair(item.id, item));
                    }
                } else if (cmd == std::string("L")) {
                    size_t oid1, oid2;
                    if (fis >> oid1 >> oid2) {
                        lines.insert(LineItem(keyMap[oid1], keyMap[oid2]));
                    }
                }
            }
            updateGLArrays();
        } else {
            std::cout << "Open failed." << std::endl;
        }
    }

    size_t DataModel::findPointByPos(const Vec &pt) {
        if (pt.isInvalid())return 0;
        for (auto &item:points) {
            if ((item.second.pt - pt).zero())
                return item.first;
        }
        return 0;
    }

}
