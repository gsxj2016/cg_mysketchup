#ifndef MYSKETCHUP_POINTPICKER_H
#define MYSKETCHUP_POINTPICKER_H

#include "vec3.hpp"
#include "geometry.hpp"
#include "openglinc.hpp"

namespace CG {

    class PointPicker {

    public:
        /**
         * 鼠标坐标到世界坐标的转换
         * @param x 鼠标x坐标
         * @param y 鼠标y坐标
         * @return 鼠标坐标对应世界坐标系中的射线
         */
        Line getPickLine(GLfloat x, GLfloat y) const;

        /**
         * 平面内取点
         * @param plane 目标平面
         * @param x 鼠标x坐标
         * @param y 鼠标y坐标
         * @param p 取点结果
         * @return true表示成功，false表示失败
         */
        bool pickPoint(const Plane &plane, int x, int y, Vec &p) const;

        /**
         * 直线上取点
         * @param line 目标直线
         * @param x 鼠标x坐标
         * @param y 鼠标y坐标
         * @param p 取点结果
         * @return true表示成功，false表示失败
         */
        bool pickPoint(const Line &line, int x, int y, Vec &p) const;

        /**
         * 获取某平面的过某点的垂线
         * @param plane 平面
         * @param pt 垂线需经过的点
         * @param out 计算结果
         * @return false表示平面无效，true表示操作成功
         */
        bool getTargetLine(const Plane &plane, const Vec &pt, Line &out) const;

    };

}

#endif //MYSKETCHUP_POINTPICKER_H
