#ifndef MYSKETCHUP_GEOMETRY_H
#define MYSKETCHUP_GEOMETRY_H

#include "vec3.hpp"

namespace CG {

    struct Line {
        Vec a, b;

        Line() {}

        Line(const Vec &a, const Vec &b) : a(a), b(b) {}

        Vec dir() const;

        /**
         * 某点到当前直线的距离
         * @param p 点
         * @return p到当前直线的距离
         */
        float distanceToLine(const Vec &p) const;

        /**
         * 直线交点
         * @param line 另一条直线
         * @param out 输出交点
         * @return true表示有交点，false表示无交点（平行、重合或异面直线）
         */
        bool intersection(const Line &line, Vec &out) const;
    };

    struct Plane {
        Vec p, n;

        Plane() {}

        Plane(const Vec &_p1, const Vec &_p2, const Vec &_p3) : p(_p1), n(((_p2 - _p1) % (_p3 - _p1)).normalize()) {}

        bool vaild() const;

        /**
         * 直线和平面交点
         * @param line 直线
         * @param out 输出交点
         * @return true表示有交点，false表示无交点
         */
        bool intersection(const Line &line, Vec &out) const;

        /**
         * 点到平面的正交投影
         * @param p1 点
         * @return 返回正交投影
         */
        Vec pointProjection(const Vec &p1) const;

    private:
        Plane(const Vec &p, const Vec &n) : p(p), n(n) {}

    };

}

#endif //MYSKETCHUP_GEOMETRY_H
