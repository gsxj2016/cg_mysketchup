#include "vec3.hpp"

namespace CG {

    Vec Vec::operator+(const Vec &rhs) const {
        return Vec(x + rhs.x, y + rhs.y, z + rhs.z);
    }

    Vec Vec::operator-(const Vec &rhs) const {
        return Vec(x - rhs.x, y - rhs.y, z - rhs.z);
    }

    Vec Vec::operator*(float rhs) const {
        return Vec(x * rhs, y * rhs, z * rhs);
    }

    Vec Vec::operator/(float rhs) const {
        return Vec(x / rhs, y / rhs, z / rhs);
    }

    float Vec::operator*(const Vec &rhs) const {
        return x * rhs.x + y * rhs.y + z * rhs.z;
    }

    Vec Vec::operator%(const Vec &rhs) const {
        return Vec(
                y * rhs.z - z * rhs.y,
                z * rhs.x - x * rhs.z,
                x * rhs.y - y * rhs.x
        );
    }

    float Vec::len() const {
        return sqrtf(*this * *this);
    }

    float Vec::len2() const {
        return *this * *this;
    }

    bool Vec::zero() const {
        return !dcmp(*this * *this);
    }

    Vec Vec::normalize() const {
        float l2 = len2();
        return dcmp(l2) ? ((*this) / sqrtf(l2)) : Vec();
    }

    Vec Vec::operator-() const {
        return Vec(-x, -y, -z);
    }

    bool Vec::isInvalid() const {
        return isnan(x) || isnan(y) || isnan(z) ||
               isinf(x) || isinf(y) || isinf(z) ||
               fabsf(x) > COORD_MAX || fabsf(y) > COORD_MAX || fabsf(z) > COORD_MAX;
    }

}

