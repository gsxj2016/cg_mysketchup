#ifndef MYSKETCHUP_OPENGLINC_HPP
#define MYSKETCHUP_OPENGLINC_HPP

#ifdef _WIN32

#include <Windows.h>

#endif

#include <GL/gl.h>
#include <GL/glu.h>

#endif //MYSKETCHUP_OPENGLINC_HPP
