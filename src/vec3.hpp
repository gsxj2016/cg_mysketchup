#ifndef CGHOMEWORK_3_VECTOR3UTIL_HPP
#define CGHOMEWORK_3_VECTOR3UTIL_HPP

#include "defs.hpp"
#include <math.h>

namespace CG {

    inline int dcmp(float x) {
        return fabsf(x) < EPS_FLOAT ? 0 : (x < 0 ? -1 : 1);
    }

    struct Vec {
        float x, y, z;

        Vec() : x(0.f), y(0.f), z(0.f) {}

        Vec(float x, float y, float z) : x(x), y(y), z(z) {}

        Vec operator+(const Vec &rhs) const;

        Vec operator-(const Vec &rhs) const;

        Vec operator*(float rhs) const;

        Vec operator/(float rhs) const;

        Vec operator-() const;

        /**
         * 向量内积
         * @param rhs 第二操作数
         * @return （*this) . rhs
         */
        float operator*(const Vec &rhs) const;

        /**
         * 向量叉积
         * @param rhs 第二操作数
         * @return (*this) x rhs
         */
        Vec operator%(const Vec &rhs) const;

        float len() const;

        float len2() const;

        bool zero() const;

        Vec normalize() const;

        bool isInvalid() const;

    };


}

#endif //CGHOMEWORK_3_VECTOR3UTIL_HPP
