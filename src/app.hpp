#ifndef MYSKETCHUP_APP_HPP
#define MYSKETCHUP_APP_HPP

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <string>
#include <cstdlib>
#include "openglinc.hpp"
#include "pointpicker.hpp"
#include "model.hpp"

namespace CG {

    const int MODE_DRAW_POINT = 0;
    const int MODE_DRAW_LINE = 1;
    const int MODE_ERASE_POINT = 2;
    const int MODE_ERASE_LINE = 3;
    const int MODE_DIVIDE = 4;
    const int MODE_EXTEND = 5;
    const int MODE_INTERSECTION = 6;

    const int PICK_PLANE_CUSTOM = -1;
    const int PICK_PLANE_XY = 0;
    const int PICK_PLANE_YZ = 1;
    const int PICK_PLANE_ZX = 2;
    const int WRAP_PICK_PLANE = 3;

    class App {
    public:
        static App &instance();

        /**
         * 程序主循环
         * @param openFileName 打开文件名（不打开文件则传入空指针）
         */
        void run(const char *openFileName = nullptr);

    private:
        PointPicker pointPicker;
        GLfloat pTra[3];
        GLfloat mRot[16];
        DataModel data;
        size_t currPoint;
        size_t hitPoint;
        size_t idpp[3];
        Vec ptpp[3];
        size_t planeCnt;
        Plane nowPlane;
        int mainMode;
        int pickPlane;
        GLfloat planeVertex[15];
        int divideCnt;
        Line nowLine;
        bool okLine;
        bool pickLinePoint;
        bool noPickFree;
        size_t sla, slb;
        bool orthographic;
        GLdouble orthoSize;
        bool renderLinesOnly;
        bool infoUpdated;
        std::string infoText;
        std::string fileName;
        int mouseX, mouseY;

        App();

        App(const App &) = delete;

        App &operator=(const App &) = delete;

        /**
         * OpenGL初始化
         */
        void initGLState();

        /**
         * 更新投影矩阵
         * @param width 窗口宽
         * @param height 窗口高
         */
        void updateProjectionMatrix(GLsizei width, GLsizei height);

        /**
         * 初始化变换矩阵
         */
        void initModelViewMatrix();

        /**
         * 绘图（由主循环调用）
         */
        void drawGLContent();

        /**
         * 绘制坐标轴
         */
        void drawAxis();

        /**
         * 绘制指示线
         */
        void drawIndicator();

        /**
         * 旋转视图
         * @param deg 角度
         * @param x 旋转轴向量x坐标
         * @param y 旋转轴向量y坐标
         * @param z 旋转轴向量z坐标
         */
        void rotateView(GLfloat deg, GLfloat x, GLfloat y, GLfloat z);

        /**
         * 平移视图
         * @param x x方向平移量
         * @param y y方向平移量
         * @param z z方向平移量
         */
        void moveView(GLfloat x, GLfloat y, GLfloat z);

        /**
         * 恢复初始视图
         */
        void resetView();

        /**
         * 处理用户的平移视图操作
         * @param et 当前帧经过的时间
         */
        void movingControl(float et);

        /**
         * 处理用户的旋转视图操作
         * @param et 当前帧经过的时间
         */
        void rotationControl(float et);

        /**
         * 处理平行投影下用户的改变视图范围操作
         * @param et 当前帧经过的时间
         * @param ws 窗口大小
         */
        void orthographicViewControl(float et, sf::Vector2u ws);

        /**
         * 鼠标左键事件
         * @param x 鼠标x坐标
         * @param y 鼠标y坐标
         */
        void processMouseLeft(int x, int y);

        /**
         * 鼠标右键事件
         * @param x 鼠标x坐标
         * @param y 鼠标y坐标
         */
        void processMouseRight(int x, int y);

        /**
         * 鼠标移动事件
         * @param x 鼠标x坐标
         * @param y 鼠标y坐标
         */
        void processMouseMove(int x, int y);

        /**
         * 选取坐标轴平面（F键）
         */
        void switchAxisPlane();

        /**
         * 获取当前取点平面
         * @return 当前平面
         */
        Plane getActivePlane();

        /**
         * 设置当前选中点
         * @param id 点ID（0表示取消选择）
         */
        void setCurrentPoint(size_t id);

        /**
         * 设置自定义取点平面的参考点
         * @param id 点ID
         */
        void addPlaneReferencePoint(size_t id);

        /**
         * 在平面或垂线上取点
         * @param x 鼠标x坐标
         * @param y 鼠标y坐标
         * @param p 取点结果
         * @return 是否成功
         */
        bool pickFreePoint(int x, int y, Vec &p);

        /**
         * 更新取点垂线
         */
        void updateTargetLine();

        /**
         * 更新垂线取点（Shift），禁止新增点（Ctrl），只渲染线框模型（Tab）等状态
         */
        void checkPickingMode();

        /**
         * 切换绘图工具后的处理
         */
        void processModeSwitch();

        /**
         * 求交模式下，设置第一条直线
         * @param a 第一个端点ID
         * @param b 第二个端点ID
         */
        void updateFirstLineSelection(size_t a, size_t b);

        /**
         * 垂线取点（Shift）模式下，标记垂足；求交模式下，线面求交
         */
        void markPointOnPlane();

        /**
         * 更新提示信息
         */
        void updateInfoText();
    };

}

#endif //MYSKETCHUP_APP_HPP
