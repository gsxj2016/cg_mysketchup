#include "pointpicker.hpp"

namespace CG {

    Line PointPicker::getPickLine(GLfloat x, GLfloat y) const {
        GLdouble mv[16], pr[16];
        GLsizei vp[4];
        GLdouble ax, ay, az, bx, by, bz;

        glGetDoublev(GL_MODELVIEW_MATRIX, mv);
        glGetDoublev(GL_PROJECTION_MATRIX, pr);
        glGetIntegerv(GL_VIEWPORT, vp);

        gluUnProject(x, GLfloat(vp[3]) - y, 0., mv, pr, vp, &ax, &ay, &az);
        gluUnProject(x, GLfloat(vp[3]) - y, 1., mv, pr, vp, &bx, &by, &bz);

        return Line(Vec((float) ax, (float) ay, (float) az), Vec((float) bx, (float) by, (float) bz));
    }

    bool PointPicker::pickPoint(const Plane &plane, int x, int y, Vec &p) const {
        if (!plane.vaild())return false;
        Line li = getPickLine((GLfloat) x, (GLfloat) y);
        return plane.intersection(li, p);
    }

    bool PointPicker::pickPoint(const Line &line, int x, int y, Vec &p) const {
        Line pkl = getPickLine((GLfloat) x, (GLfloat) y);
        Plane plane(line.a, line.b, pkl.a);
        if (plane.vaild()) {
            pkl.b = plane.pointProjection(pkl.b);
            return line.intersection(pkl, p);
        } else {
            p = pkl.a;
            return true;
        }
    }

    bool PointPicker::getTargetLine(const Plane &plane, const Vec &pt, Line &out) const {
        if (!plane.vaild())return false;
        out = Line(pt, pt + plane.n);
        return true;
    }

}
