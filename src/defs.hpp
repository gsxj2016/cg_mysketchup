#ifndef MYSKETCHUP_DEFS_HPP
#define MYSKETCHUP_DEFS_HPP

#define WINDOW_INITIAL_WIDTH  800
#define WINDOW_INITIAL_HEIGHT 600

#define MOVE_PER_SEC 50.f
#define ROT_PER_SEC 22.5f
#define ORTHO_MOVE_PER_SEC 10.f
#define COORD_MAX 8192.f
#define EPS_FLOAT 1e-4
#define PICK_DIST 2.f
#define POINT_DRAW_SIZE 8.f

#endif //MYSKETCHUP_DEFS_HPP
