#include "geometry.hpp"

namespace CG {

    Vec Line::dir() const {
        return b - a;
    }

    float Line::distanceToLine(const Vec &p) const {
        Vec d = dir();
        return (d % (p - a)).len() / d.len();
    }

    bool Line::intersection(const Line &line, Vec &out) const {
        Vec ab = dir(), cd = line.dir(), ac = line.a - a;
        float denom = (ab % cd).len();
        if (0 == dcmp(denom))return false;
        out = line.a + cd * ((ac % ab).len() / denom);
        return (ab % (out - a)).zero();
    }

    bool Plane::vaild() const {
        return !n.zero();
    }

    bool Plane::intersection(const Line &line, Vec &out) const {
        Vec a = line.a, d = line.dir();
        float dl = d * n;
        out = a + d * (((p - a) * n) / dl);
        return dcmp(dl) != 0;
    }

    Vec Plane::pointProjection(const Vec &p1) const {
        Vec v = p1 - p;
        return p1 - n * (n * v);
    }


}
