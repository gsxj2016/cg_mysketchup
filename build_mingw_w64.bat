@echo off
echo Required software:
echo     64-bit Windows
echo     MinGW-w64 GCC 6.x (With PATH set)
echo     CMake 3.7+ (With PATH set)
pause
echo on

md build_mingw
cd build_mingw
cmake -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Release ..
mingw32-make -j8
copy ..\third_party\SFML-2.4.2-MINGW64\bin\sfml-graphics-2.dll .\
copy ..\third_party\SFML-2.4.2-MINGW64\bin\sfml-window-2.dll .\
copy ..\third_party\SFML-2.4.2-MINGW64\bin\sfml-system-2.dll .\
copy ..\third_party\DroidSansFallbackFull.ttf .\
start ..\build_mingw
pause